import 'react-native-gesture-handler';
import React from 'react'
import { Text, View } from 'react-native'

import Tabbar from './src/navigation/Tabbar';

import { NavigationContainer } from '@react-navigation/native';


const App = () => {
    return (
        <NavigationContainer>
            <Tabbar/>
        </NavigationContainer>
    )
}

export default App
