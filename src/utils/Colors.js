const Colors = {
    RED: '#FF0000',
    BLACK: '#000000',
    //WHITE: '#FFFFFF',
    GRAY: '#A9A9A9',
    YELLOW: '#f2b443',
    LIGHT_YELLOW: '#f2ba5f',


    
    BLUE: '#004FB4',
    GREEN: '#55CB82',
    LIGHT_GREEN: '#CAEFD8',
    DARK_GREEN: '#16A64d',

    PINK: '#FE93B3',
    LIGHT_PINK: '#EED2DF',
    DARK_PINK: '#E2006A',

    GREY: '#A4B8D3',
    LIGHT_GREY: '#CBD2E1',
    DARK_GREY: '#4F6C92',

    WHITE: '#fff',
    OFF_WHITE: "#F1F4F8",
};

export default Colors;
