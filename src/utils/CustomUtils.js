import moment from 'moment'

export const showDate = (date, format) => {
  return moment(date).format('DD/MM/YYYY')
}

export const showTime = (date, format) => {
  return moment(date).format('hh:mm')
}