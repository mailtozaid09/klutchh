import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import MyShifts from '../screens/MyShifts';
import AvailableShifts from '../screens/AvailableShifts';

import Colors from '../utils/Colors';


const Tab = createBottomTabNavigator();


export default function Tabbar() {

    LogBox.ignoreAllLogs(true)
    
    useEffect(() => {
        //console.log("Tabbar");
    }, []);

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarStyle: {height: 60, backgroundColor: Colors.WHITE},
                tabBarIcon: ({ focused, color, size }) => {
                let routeName;
    
                if (route.name === 'MyShifts') {
                    routeName = 'My shifts'
                } else if (route.name === 'AvailableShifts') {
                    routeName = 'Available shifts'
                } 
                return(
                    <View style={{alignItems: 'center'}} >
                        <Text style={{fontSize: 16, color: focused ? Colors.BLUE : Colors.GREY}} >{routeName}</Text>
                    </View>
                );
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
            })}
        >
            <Tab.Screen
                name="MyShifts"
                component={MyShifts}
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                     // console.log("-----");
                      e.preventDefault();
                      navigation.navigate('MyShifts', {screen: 'MyShifts', })
                    },
                  })}
                options={{
                    headerShown: true,
                    headerTitle: 'My shifts',
                    tabBarShowLabel: false
                }}
            />
            <Tab.Screen
                name="AvailableShifts"
                component={AvailableShifts}
                
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                      //console.log("-----");
                      e.preventDefault();
                      navigation.navigate('AvailableShifts', {screen: 'AvailableShifts', })
                    },
                  })}
                options={{
                    headerShown: true,
                    headerTitle: 'Available shifts',
                    tabBarShowLabel: false
                }}
            />
        </Tab.Navigator>
    );
}