import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, FlatList, TouchableOpacity, ActivityIndicator, } from 'react-native'
import { getAllShifts, updateShiftStatus } from '../api/ApiManager';
import { showDate, showTime } from '../utils/CustomUtils';

import Colors from '../utils/Colors';
import { availableShift } from '../mockData/data';



const AvailableShifts = () => {

    const [currentTab, setCurrentTab] = useState('Helsinki');

    const [availableShifts, setAvailableShifts] = useState(availableShift);

    const [groupedShifts, setGroupedShifts] = useState([]);

    const [groupedByAreaShifts, setGroupedByAreaShifts] = useState([]);

    const [currentBtnId, setCurrentBtnId] = useState('');
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            groupedByAreaFunction()
            groupedShiftsFunction()
        }, 500);

    }, [])


    const groupedByAreaFunction = () =>  {
        const groupedByArea = availableShifts && availableShifts.reduce((item, shifts) => {
            const { area } = shifts;
            item[area] = item[area] ?? [];
            item[area].push(shifts);
            return item;
        }, {});

        setGroupedByAreaShifts(groupedByArea)
    }


    const groupedShiftsFunction = () =>  {
        const groupByDate = availableShifts && availableShifts.reduce((item, shifts) => {
            const { shiftDate } = shifts;
            item[shiftDate] = item[shiftDate] ?? [];
            item[shiftDate].push(shifts);
            return item;
        }, {});

        setGroupedShifts(groupByDate)
        setLoader(false)
    }

    const updateShift = (item, status) => {

        setTimeout(() => {
            setCurrentBtnId('')
        }, 1000);
        console.log("item  ", item, status);

        const newArr = availableShifts.map(obj => {
            if (obj.id === item.id) {
              return {...obj, booked: !status};
            }
          
            return obj;
          });
        
          console.log(newArr);

          setAvailableShifts(newArr)

          groupedShiftsFunction()
    }

    
    return (
        <View style={styles.container} >
            {loader 
            ?
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                <ActivityIndicator size="large" color={Colors.BLUE} />
            </View>
            :
            <View>
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', backgroundColor: Colors.WHITE, borderTopWidth: 1, borderColor: Colors.LIGHT_GREY, paddingVertical: 20}} >
                {Object.keys(groupedByAreaShifts).map((item) => {
                    return(
                        <TouchableOpacity onPress={() => setCurrentTab(item)} style={{}} >
                            <Text style={{color: item == currentTab ? Colors.DARK_GREY : Colors.LIGHT_GREY, fontWeight: 'bold', fontSize: 16}} >{item} ({groupedByAreaShifts[item].length})</Text>
                        </TouchableOpacity>
                    )
                })}
            </View>

            <FlatList
                data={Object.keys(groupedShifts)}
                renderItem={({item})=> {
                    return(
                        <View>
                            <View style={styles.dateHeader} >
                                <Text style={styles.dateText} >{item}</Text>
    
                                <View style={styles.rowStyle} >
                                    <Text style={{color: Colors.GREY}} >{groupedShifts[item].length} shifts </Text>
                                </View>
                            </View>
                            {groupedShifts[item].map((element) => (
                                <View>
                                    {element.area == currentTab
                                    ?
                                    <View style={styles.shiftContainer} >
                                        <View>
                                            <Text style={{color: Colors.DARK_GREY}} >{element.startTime} - {element.endTime}</Text>
                                            <Text style={{color: Colors.GREY}}>{element.area}</Text>
                                        </View>
                                        <View style={styles.rowStyle} >
                                            {element.booked
                                            ?
                                            <View>
                                                <Text style={styles.bookedText} >Booked</Text>
                                            </View>
                                            :null
                                            }

                                            {element.overlapping
                                            ?
                                            <View>
                                                <Text style={styles.overlappingText} >Overlapping</Text>
                                            </View>
                                            :null
                                            }

                                            {element.booked
                                            ?
                                            <TouchableOpacity 
                                                onPress={() => {setCurrentBtnId(element.id); updateShift(element, false)}}
                                                style={styles.cancelButton} >
                                                    {currentBtnId == element.id 
                                                    ? 
                                                    <ActivityIndicator size='small' color={Colors.DARK_PINK} />
                                                    : 
                                                    <Text style={styles.cancelText} >Cancel</Text>
                                                    }
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity 
                                                disabled={element.overlapping ? true : false}
                                                onPress={() => {setCurrentBtnId(element.id); updateShift(element, true)}}
                                                style={element.overlapping ? styles.disabledButton : styles.bookButton} >
                                                    {currentBtnId == element.id 
                                                    ? 
                                                    <ActivityIndicator size='small' color={Colors.DARK_GREEN} />
                                                    : 
                                                    <Text style={element.overlapping ? styles.disabledText : styles.bookText} >Book</Text>
                                                    }
                                            </TouchableOpacity>
                                            }
                                        </View>
                                    </View>
                                    :
                                    null
                                    }
                                    
                            </View>
                            ))}
                        </View>
                    )
                }}
            />
        </View>
        }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center'
    },
    shiftContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center', padding: 10,
        borderBottomWidth: 1,
        borderColor: Colors.LIGHT_GREY,
        backgroundColor: Colors.WHITE,
    },
    rowStyle: {
        flexDirection: 'row',
        alignItems: 'center', 
    },
    dateText: {
        color: Colors.DARK_GREY,
        fontWeight: 'bold', 
        marginRight: 20
    },
    dateHeader: {
        padding: 10, 
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.OFF_WHITE,
    },
    overlappingText: {
        color: Colors.DARK_PINK,
        fontWeight: 'bold',
        marginRight: 20,
    },
    bookedText: {
        color: Colors.DARK_GREY,
        fontWeight: 'bold',
        marginRight: 20,
    },
    cancelText: {
        color: Colors.DARK_PINK,
        fontWeight: 'bold',
    },
    cancelButton: {
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: Colors.PINK,
        borderRadius: 20, padding: 8,
        paddingHorizontal: 25,
    },
    bookText: {
        color: Colors.DARK_GREEN,
        fontWeight: 'bold',
    },
    bookButton: {
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: Colors.GREEN,
        borderRadius: 20, padding: 8,
        paddingHorizontal: 25,
    },
    disabledText: {
        color: Colors.GRAY,
        fontWeight: 'bold',
    },
    disabledButton: {
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: Colors.GRAY,
        borderRadius: 20, padding: 8,
        paddingHorizontal: 25,
    },
})


export default AvailableShifts
