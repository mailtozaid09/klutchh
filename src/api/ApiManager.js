import { APP_URL } from './ApiConstants'
import commonFetch from './commanApi'

{/* Get Shifts */}

export function getAllShifts (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/shifts`,
            //data: data,
            method: 'GET'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

export function updateShiftStatus (id, data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/${id}`,
            data: data,
            method: 'PUT'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}


